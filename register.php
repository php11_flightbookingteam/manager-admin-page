<html>
    <head>
        <meta charset = "UTF-8">
        <title></title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="jumbotron">
            <h1>Welcome to Test PHP11 Registration page</h1> 
            <p>Making the best web in the world</p> 
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="./register.php">Registration</a></li> 
                    <li><a href="./login.php">Log in</a></li> 
                </ul>
            </div>
        </nav>
        <div class="container">
            <form class="form-horizontal" action="register.php" method="POST">
                <input type="hidden" name="action" value="register">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="username">Username:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" name="username" placeholder="Enter username">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password:</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="pwd"  name="password" placeholder="Enter password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="repassword">Re-enter Password:</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="pwd" name="repassword" placeholder="Enter password">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="fullname">fullname:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email"  name="fullname" placeholder="Enter Fullname">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" name="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

                <?php
//                require '../model/database.php';
                require './model/form_validation.php';
                if (isset($_POST['submit'])) {
                    $username = $_POST['username'];
                    $password = $_POST['password'];
                    $repassword = $_POST['repassword'];
                    $fullname = $_POST['fullname'];

                    $blank_check = is_all_fill();
                    $user_check = is_valid_name($username);
                    $pass_check = is_valid_pass($password, $repassword);

                    if ($blank_check != "") {
                        echo "<div class=\"alert alert-danger\"><strong>Sorry!</strong> $blank_check</div>";
                    } else if ($user_check != "") {
                        echo "<div class=\"alert alert-danger\"><strong>Sorry!</strong> $user_check</div>";
                    } else if ($pass_check != "") {
                        echo "<div class=\"alert alert-danger\"><strong>Sorry!</strong> $pass_check</div>";
                    } else {
                        insert($username, $password, $fullname);
                        echo "<div class=\"alert alert-success\"><strong>Success!</strong> Registration Success! Please Login to continue <a href='./login.php'>Login</a></div>";
//                        header("Location: ../login.php");
                    }
                }
                ?>

            </form>
        </div>

    </body>
</html>
