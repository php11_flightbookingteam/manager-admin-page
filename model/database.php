<?php

function connect_db() {
    $dns = "mysql:host=localhost;dbname=php_test";
    $username = "root";
    $password = "";
    $option = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
    $db = new PDO($dns, $username, $password, $option);
    return $db;
}

function insert($name, $type, $numSeat, $numVipSeat, $createDate, $status = 1) {
    global $db;
    $sql = "INSERT INTO `php_test`.`user_info` (`id`, `username`, `password`, `fullname`) VALUES (NULL, ?, ?, ?);";
    $command = $db->prepare($sql);
    $command->bindValue(1, $name);
    $command->bindValue(2, $pass);
    $command->bindValue(3, $fullname);
    $result = $command->execute();
    return $result;
}

function findAllCatgory() {
    global $db;
    $sql = "SELECT * FROM category";
    $command = $db->prepare($sql);
    $command->execute();
    $result = $command->fetchAll();
    return $result;
}

function findById($category_id) {
    global $db;
    $sql = "SELECT * FROM article WHERE category_id = ?";
    $command = $db->prepare($sql);
    $command->bindValue(1, $category_id);
    $command->execute();
    $result = $command->fetchAll();
    return $result;
}

function findByName($name) {
    global $db;
    $sql = "SELECT * FROM user_info WHERE name like ?";
    $command = $db->prepare($sql);
    $command->bindValue(1, '%' . $name . '%');
    $command->execute();
    $result = $command->fetchAll();
    return $result;
}

function findByUsername($username) {
    global $db;
    $db = connect_db();
    $sql = "SELECT * FROM user_info WHERE username = ?";
    $command = $db->prepare($sql);
    $command->bindValue(1, $username);
    $command->execute();
    $result = $command->fetchAll();
    return $result;
}

function login($user, $pass) {
    global $db;
    $db = connect_db();
    $sql = "SELECT * FROM user_info WHERE username = ? AND password = ?";
    $command = $db->prepare($sql);
    $command->bindValue(1, $user);
    $command->bindValue(2, $pass);
    $command->execute();
    $result = $command->fetch();
    return $result;
}
