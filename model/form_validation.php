<?php

require_once 'database.php';

function is_all_fill() {
    $require = array('username', 'password', 'repassword', 'fullname');
    foreach ($require as $field) {
        if (empty($_POST[$field])) {
            return "All fields must be filled";
        }
    }
    return "";
}

function is_valid_name($username) {
    $result = findByUsername($username);
    if ($result == false) {
        $pattern = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/";
        $kq = preg_match($pattern, $username);
        if ($kq == 0) {
            return "Username must contain more than eight letters and at least 1 number.";
        }
        //    http://stackoverflow.com/questions/19605150/regex-for-password-must-be-contain-at-least-8-characters-least-1-number-and-bot
    } else {
        return "Username already exists!";
    }
    return "";
}

function is_valid_pass($str_pass, $str_repass) {
    if ($str_pass != $str_repass) {
        return "Password and re-enter password must be the same";
    } else {
        $pattern = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/";
        $kq = preg_match($pattern, $str_pass);
        if ($kq == 0) {
            return "Password must contain more than eight letters and at least 1 number.";
        }
    }
    return "";
}

function is_valid_email($email) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return "Email wrong format";
    }
    return "";
}
