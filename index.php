<?php

require_once './model/database.php';

$db = connect_db();
$action = "listCat";

if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

if ($action == "listCat") {
    $result = findAllCatgory();
    include './view/category_list.php';
} else if ($action == "listArt"){
    $category_id = $_GET['category_id'];
    $result = findById($category_id);
    include './view/article_list.php';
}