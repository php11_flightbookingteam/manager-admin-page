<html>
    <head>
        <title>Login Page</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>
        <div class="jumbotron">

            <h1>werwerwer</h1> 
            <p>Making the best web in the world</p> 
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="./register.php">Registration</a></li> 
                    <li><a href="#">Log out</a></li> 
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <form class="form" action="login.php" method="POST">
                        <div class="form-group">
                            <label for="name">Username</label>
                            <input class="form-control" type="text" name="username">
                        </div>
                        <div class="form-group">
                            <label for="pass">Password</label>
                            <input class="form-control" type="password" name="password">
                        </div>
                        <input type="submit" name="submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
            <?php
            session_start();
            
            require_once './model/database.php';
            $db = connect_db();

            if (isset($_POST['submit'])) {
                $user = $_POST['username'];
                $pass = $_POST['password'];
                $result = login($user, $pass);
                if ($result == false) {
                    echo "User hoặc Pass không đúng";
                } else {
                    $_SESSION['username'] = $user;
                    $_SESSION['password'] = $pass;
                    header('Location: ./index.php');
                }
            }
            ?>
        </div>

    </body>
</html>