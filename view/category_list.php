<!DOCTYPE html>
<html>
    <head>
        <meta charset ="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <div class="jumbotron">
            <h1>PHP Home Page</h1> 
            <p>Making the best web in the world</p> 
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="./register.php">Registration</a></li>
                    <li><a href="./login.php">Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <table class="table table-bordered">
                        <tr>
                            <td>Category ID</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                        foreach ($result as $row):
                            ?>
                            <tr>
                                <td><?= $row['id'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['description'] ?></td>
                                <td>
                                    <a href="index.php?action=listArt&category_id=<?= $row['id'] ?>">
                                        View
                                    </a>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>

